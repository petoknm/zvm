#!/usr/bin/env bash
set -e

curl http://infocom-if.org/downloads/zork1.zip -o zork1.zip
# curl http://infocom-if.org/downloads/zork2.zip -o zork2.zip
# curl http://infocom-if.org/downloads/zork3.zip -o zork3.zip

unzip -p zork1.zip DATA/ZORK1.DAT > test_data/zork1.z3
# unzip -p zork2.zip DATA/ZORK2.DAT > test_data/zork2.z3
# unzip -p zork3.zip DATA/ZORK3.DAT > test_data/zork3.z3

rm -f zork*.zip
