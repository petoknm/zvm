extern crate log;
extern crate env_logger;
extern crate zvm;

use std::io;
use zvm::{ Zvm, Program };

fn main() {
    env_logger::init();
    let bytes = include_bytes!("../test_data/zork1.z3");
    let program = Program::new(bytes.to_vec());
    let out = Box::new(io::stdout());
    let mut zvm = Zvm::new(out, program);
    let mut input = None;
    loop {
        zvm.run(input);
        let mut s = String::new();
        input = io::stdin().read_line(&mut s).map(|_| s).ok();
    }
}
