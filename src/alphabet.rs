pub struct Alphabet(pub [String; 3]);

impl Default for Alphabet {
    fn default() -> Self {
        Alphabet([
            " .....abcdefghijklmnopqrstuvwxyz".to_string(),
            " .....ABCDEFGHIJKLMNOPQRSTUVWXYZ".to_string(),
            " ......\n0123456789.,!?_#'\"/\\-:()".to_string()
        ])
    }
}

impl Alphabet {
    pub fn index(&self, a: u8, c: u8) -> char {
        self.0[a as usize].chars().nth(c as usize).unwrap()
    }
}
