use crate::program::Program;
use crate::alphabet::Alphabet;
use crate::parser::dictionary::dictionary;
use crate::string_decoding::decode;

#[derive(Clone, Default)]
pub struct Entry {
    // Byte address of the entry in memory
    pub address: u16,
    pub text: String,
}

pub struct Dictionary {
    pub word_separators: Vec<char>,
    pub entries: Vec<Entry>,
}

impl Dictionary {
    pub fn find(&self, s: &str) -> Option<Entry> {
        self.entries.iter().cloned().find(|e| e.text == s)
    }
}

impl Dictionary {
    pub fn parse(p: &Program, alphabet: &Alphabet) -> Dictionary { // TODO remove Program
        let (_, pd) = dictionary(p.dictionary()).unwrap();
        let header_size = pd.header.word_separators.len() as u16 + 4;
        let entry_size = pd.header.entry_length as u16;
        let entry_addr = |i| p.header.dictionary_base + header_size + i*entry_size;
        Dictionary{
            word_separators: pd.header.word_separators.iter().cloned()
                .map(|v| v.into())
                .collect(),
            entries: pd.entries.iter()
                .map(|zs| decode(zs, alphabet, None).unwrap())
                .enumerate()
                .map(|(i, s)| Entry{
                    address: entry_addr(i as u16),
                    text: s
                })
                .collect()
        }
    }
}
