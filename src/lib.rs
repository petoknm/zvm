#![feature(try_from, ptr_offset_from)]
#![recursion_limit="256"]

#[macro_use] extern crate log;
#[macro_use] extern crate nom;
#[macro_use] extern crate derive_error;
#[macro_use] extern crate cascade;
extern crate bytes;

mod parser;
mod program;
mod zvm;
mod stack;
mod alphabet;
mod objects;
mod string_decoding;
mod dictionary;

pub use self::zvm::Zvm;
pub use self::program::Program;
