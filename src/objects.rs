use crate::parser::strings::Zstring;
use crate::parser::objects::{PropertyTable, ObjectTable};
use crate::parser::header::Header;

pub fn get_property_data_mut<'a>(dynamic_memory: &'a mut [u8], h: &Header, oid: u8, pid: u8) -> Option<&'a mut [u8]> {
    let pt_addr = ObjectTable(&mut dynamic_memory[h.object_table_base as usize..]).object(oid).property_table_addr();
    Some(PropertyTable(&mut dynamic_memory[pt_addr as usize..]).property(pid)?.data_mut())
}

pub fn get_property_data_addr<'a>(dynamic_memory: &'a mut [u8], h: &Header, oid: u8, pid: u8) -> Option<u16> {
    let pt_addr = ObjectTable(&mut dynamic_memory[h.object_table_base as usize..]).object(oid).property_table_addr();
    Some(unsafe {
        let d = PropertyTable(&mut dynamic_memory[pt_addr as usize..]).property(pid)?.data_mut().as_ptr();
        d.offset_from(dynamic_memory.as_ptr()) as u16
    })
}

pub fn get_property_default<'a>(dynamic_memory: &'a mut [u8], h: &Header, pid: u8) -> u16 {
    ObjectTable(&mut dynamic_memory[h.object_table_base as usize..]).property_defaults()[pid as usize]
}

pub fn get_attr(dynamic_memory: &mut [u8], h: &Header, oid: u8, aid: usize) -> bool {
    let byte_n = aid / 8;
    let bit_n = 7 - (aid % 8);

    let flags = ObjectTable(&mut dynamic_memory[h.object_table_base as usize..]).object(oid).flags_mut();
    (flags[byte_n] & (1 << bit_n)) != 0
}

pub fn set_attr(dynamic_memory: &mut [u8], h: &Header, oid: u8, aid: usize, value: bool) {
    let byte_n = aid / 8;
    let bit_n = 7 - (aid % 8);

    let flags = ObjectTable(&mut dynamic_memory[h.object_table_base as usize..]).object(oid).flags_mut();
    if value {
        flags[byte_n] |= 1 << bit_n;
    } else {
        flags[byte_n] &= !(1 << bit_n);
    }
}

pub fn insert(dynamic_memory: &mut [u8], h: &Header, s_oid: u8, d_oid: u8) {
    // Object(s_oid).parent = d_oid
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(s_oid).parent() = d_oid;
    // let c_oid = Object(d_oid).child
    let c_oid = *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(d_oid).child();
    // Object(d_oid).child = s_oid
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(d_oid).child() = s_oid;
    // Object(s_oid).sibling = c_oid
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(s_oid).sibling() = c_oid;
}

pub fn is_parent_of(dynamic_memory: &mut [u8], h: &Header, p_oid: u8, c_oid: u8) -> bool {
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(c_oid).parent() == p_oid
}

pub fn get_parent(dynamic_memory: &mut [u8], h: &Header, oid: u8) -> u8 {
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(oid).parent()
}

pub fn get_child(dynamic_memory: &mut [u8], h: &Header, oid: u8) -> u8 {
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(oid).child()
}

pub fn get_sibling(dynamic_memory: &mut [u8], h: &Header, oid: u8) -> u8 {
    *ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(oid).sibling()
}

pub fn get_name(dynamic_memory: &mut [u8], h: &Header, oid: u8) -> Zstring {
    let pt_addr = ObjectTable(&mut dynamic_memory[h.object_table_base as usize ..]).object(oid).property_table_addr();
    PropertyTable(&mut dynamic_memory[pt_addr as usize..]).name()
}
