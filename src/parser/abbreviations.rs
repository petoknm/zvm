use nom::be_u16;

pub struct AbbrevTable {
    pub entries: [u16; 96]
}

named!(pub abbrev_table<AbbrevTable>, do_parse!(
    entries: count_fixed!(u16, be_u16, 96) >>
    (AbbrevTable{entries})
));
