use crate::parser::strings::{zstring_fixed, Zstring};
use nom::{be_u8, be_u16};

#[derive(Debug, PartialEq)]
pub struct Dictionary {
    pub header: DictionaryHeader,
    pub entries: Vec<Zstring>,
}

#[derive(Debug, PartialEq)]
pub struct DictionaryHeader{
    pub word_separators: Vec<u8>,
    pub entry_length: u8,
    pub entry_count: u16,
}

named!(dictionary_header<DictionaryHeader>, do_parse!(
    word_separators: length_count!(be_u8, be_u8) >>
    entry_length: be_u8 >>
    entry_count: be_u16 >>
    (DictionaryHeader{
        word_separators,
        entry_length,
        entry_count,
    })
));

named_args!(dictionary_entry(entry_length: usize)<Zstring>, do_parse!(
    zs: apply!(zstring_fixed, 2) >>
    take!(entry_length - 4) >>
    (zs)
));

named!(pub dictionary<Dictionary>, do_parse!(
    header: dictionary_header >>
    entries: count!(
        apply!(dictionary_entry, header.entry_length as usize),
        header.entry_count as usize) >>
    (Dictionary{ header, entries })
));

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::strings::zword;

    #[test]
    fn dictionary_header_works_czech() {
        let (_, dh) = dictionary_header(&include_bytes!("../../test_data/czech.z3")[2075..]).unwrap();
        assert_eq!(dh, DictionaryHeader{
            word_separators: vec![46, 44, 34],
            entry_length: 7,
            entry_count: 0,
        });
    }

    #[test]
    fn dictionary_header_works_zork1() {
        let (_, dh) = dictionary_header(&include_bytes!("../../test_data/zork1.z3")[15137..]).unwrap();
        assert_eq!(dh, DictionaryHeader{
            word_separators: vec![44, 46, 34],
            entry_length: 7,
            entry_count: 697,
        });
    }

    #[test]
    fn dictionary_works_zork1() {
        let (_, Dictionary{header, entries}) = dictionary(&include_bytes!("../../test_data/zork1.z3")[15137..]).unwrap();
        assert_eq!(header, DictionaryHeader{
            word_separators: vec![44, 46, 34],
            entry_length: 7,
            entry_count: 697,
        });
        assert_eq!(entries[0], Zstring(vec![
            zword(5, 6, 1),
            zword(4, 27, 10)
        ]));
    }
}
