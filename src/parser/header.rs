use nom::{be_u8, be_u16};

#[derive(Debug, PartialEq)]
pub struct Header {
    pub version: u8,
    pub high_memory_base: u16,
    pub pc: u16,
    pub dictionary_base: u16,
    pub object_table_base: u16,
    pub global_table_base: u16,
    pub static_memory_base: u16,
    pub abbrev_table_base: u16,
    pub length: u16,
    pub checksum: u16,
}

named!(version<u8>, verify!(be_u8, |v| v == 3));

named!(pub header<Header>, do_parse!(
    version: version >>
    take!(3) >>
    high_memory_base: be_u16 >>
    pc: be_u16 >>
    dictionary_base: be_u16 >>
    object_table_base: be_u16 >>
    global_table_base: be_u16 >>
    static_memory_base: be_u16 >>
    take!(8) >>
    abbrev_table_base: be_u16 >>
    length: be_u16 >>
    checksum: be_u16 >>
    (Header{
        version,
        high_memory_base,
        pc,
        dictionary_base,
        object_table_base,
        global_table_base,
        static_memory_base,
        abbrev_table_base,
        length,
        checksum,
    })
));

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn version_works() {
        assert_eq!(version(&[3]), Ok((&[][..], 3)));
        assert!(version(&[4]).is_err());
    }

    #[test]
    fn header_works_czech() {
        let (_, h) = header(include_bytes!("../../test_data/czech.z3")).unwrap();
        assert_eq!(h, Header {
            version: 3,
            high_memory_base: 2082,
            pc: 2083,
            dictionary_base: 2075,
            object_table_base: 270,
            global_table_base: 1337,
            static_memory_base: 2073,
            abbrev_table_base: 70,
            length: 5197,
            checksum: 59222,
        });
    }

    #[test]
    fn header_works_zork1() {
        let (_, h) = header(include_bytes!("../../test_data/zork1.z3")).unwrap();
        assert_eq!(h, Header {
            version: 3,
            high_memory_base: 20023,
            pc: 20229,
            dictionary_base: 15137,
            object_table_base: 688,
            global_table_base: 8817,
            static_memory_base: 11859,
            abbrev_table_base: 496,
            length: 42438,
            checksum: 41257
        });
    }
}
