pub mod var_op;
pub mod two_op;
pub mod one_op;
pub mod zero_op;

#[cfg(test)]
mod tests;

use std::convert::TryFrom;
use nom::{be_u8, be_u16, IResult};

use crate::parser::strings::Zstring;
use self::zero_op::zero_op;
use self::one_op::one_op;
use self::var_op::var_op;
use self::two_op::two_op;

#[derive(PartialEq, Debug, Clone)]
pub struct LargeConstant(pub u16);
#[derive(PartialEq, Debug, Clone)]
pub struct SmallConstant(pub u8);
#[derive(PartialEq, Debug, Clone)]
pub struct Variable(pub u8);

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Operand {
    Large(u16),
    Small(u8),
    Variable(u8),
}

impl Operand {
    pub fn to_variable(self) -> Operand {
        match self {
            Operand::Small(s) => Operand::Variable(s),
            _ => unimplemented!(),
        }
    }
}

impl TryFrom<Operand> for u16 {
    type Error = Variable;
    fn try_from(o: Operand) -> Result<u16, Self::Error> {
        match o {
            Operand::Large(l) => Ok(l),
            Operand::Small(s) => Ok(s as u16),
            Operand::Variable(v) => Err(Variable(v)),
        }
    }
}

impl From<LargeConstant> for Operand {
    fn from(lc: LargeConstant) -> Operand {
        Operand::Large(lc.0)
    }
}

impl From<Variable> for Operand {
    fn from(v: Variable) -> Operand {
        Operand::Variable(v.0)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Store(pub u8);
#[derive(PartialEq, Debug, Clone)]
pub struct Branch {
    pub condition: bool,
    pub offset: i16,
}

named!(large_constant<LargeConstant>, do_parse!(c: be_u16 >> (LargeConstant(c))));
named!(small_constant<SmallConstant>, do_parse!(c: be_u8 >> (SmallConstant(c))));
named!(variable<Variable>, do_parse!(v: be_u8 >> (Variable(v))));

named_args!(operand_types(n: usize)<Vec<Operand>>, bits!(
    fold_many_m_n!(n, n, take_bits!(u8, 2), vec![], |mut args: Vec<_>, a| {
        let a = match a {
            0b00 => Some(Operand::Large(0)),
            0b01 => Some(Operand::Small(0)),
            0b10 => Some(Operand::Variable(0)),
            _    => None
        };
        if let Some(a) = a {
            args.push(a);
        }
        args
    })
));

fn operands(mut i: &[u8], n: usize) -> IResult<&[u8], Vec<Operand>> {
    let r = operand_types(i, n)?;
    i = r.0;
    let mut v = vec![];

    for t in r.1 {
        v.push(match t {
            Operand::Large(_) => {
                let r = be_u16(i)?;
                i = r.0;
                Operand::Large(r.1)
            },
            Operand::Small(_) => {
                let r = be_u8(i)?;
                i = r.0;
                Operand::Small(r.1)
            }
            Operand::Variable(_) => {
                let r = be_u8(i)?;
                i = r.0;
                Operand::Variable(r.1)
            }
        });
    }

    Ok((i, v))
}

named!(op4<Vec<Operand>>, apply!(operands, 4));

named!(store<Store>, do_parse!(
    v: be_u8 >> (Store(v))
));

named!(branch_short<Branch>, bits!(do_parse!(
    condition: take_bits!(u8, 1) >>
    tag_bits!(u8, 1, 1) >>
    offset: take_bits!(u8, 6) >>
    (Branch{condition: condition != 0, offset: offset as i16})
)));

named!(branch_long<Branch>, bits!(do_parse!(
    condition: take_bits!(u8, 1) >>
    tag_bits!(u8, 1, 0) >>
    offset: take_bits!(i16, 14) >>
    (Branch{condition: condition != 0, offset})
)));

named!(branch<Branch>, alt!(branch_short | branch_long));

#[derive(PartialEq, Debug, Clone)]
pub enum Instruction {
    // ZeroOp
    Rtrue, Rfalse, Print(Zstring), PrintRet(Zstring), Nop,
    Save(Branch), Restore(Branch), Restart, RetPopped, Pop, Quit,
    NewLine, ShowStatus, Verify(Branch),

    // OneOp
    Jz(Operand, Branch),
    GetSibling(Operand, Store, Branch),
    GetChild(Operand, Store, Branch),
    GetParent(Operand, Store),
    GetPropLen(Operand, Store),
    Inc(Operand),
    Dec(Operand),
    PrintAddr(Operand),
    RemoveObj(Operand),
    PrintObj(Operand),
    Ret(Operand),
    Jump(Operand),
    PrintPaddr(Operand),
    Load(Operand, Store),

    // TwoOp(two_op::Instruction),
    Je(Operand, Operand, Branch),
    Jg(Operand, Operand, Branch),
    DecChk(Operand, Operand, Branch),
    IncChk(Operand, Operand, Branch),
    Jin(Operand, Operand, Branch),
    Test(Operand, Operand, Branch),
    Or(Operand, Operand, Store),
    And(Operand, Operand, Store),
    TestAttr(Operand, Operand, Branch),
    SetAttr(Operand, Operand),
    ClearAttr(Operand, Operand),
    Store(Operand, Operand),
    InsertObj(Operand, Operand),
    LoadW(Operand, Operand, Store),
    LoadB(Operand, Operand, Store),
    GetProp(Operand, Operand, Store),
    GetPropAddr(Operand, Operand, Store),
    Add(Operand, Operand, Store),
    Sub(Operand, Operand, Store),
    Mul(Operand, Operand, Store),
    Div(Operand, Operand, Store),
    Mod(Operand, Operand, Store),

    // VarOp
    Call(Operand, Vec<Operand>, Store),
    StoreW(Operand, Operand, Operand),
    StoreB(Operand, Operand, Operand),
    PutProp(Operand, Operand, Operand),
    Sread(Operand, Operand),
    PrintChar(Operand),
    PrintNum(Operand),
    Push(Operand),
    Pull(Operand),
}

named!(pub instruction<Instruction>, alt!(
    zero_op | var_op | one_op | two_op
));

named!(instructions<Vec<Instruction>>, many0!(instruction));
