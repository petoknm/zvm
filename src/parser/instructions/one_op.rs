use nom::{be_u8, be_u16};

use crate::parser::instructions::Instruction::*;
use crate::parser::instructions::{store, branch, Instruction, Operand};

named_args!(arg(opcode: u8)<Operand>, do_parse!(
    tag!([opcode]) >>
    o: switch!(value!(opcode),
        0x80...0x8f => map!(be_u16, |l| Operand::Large(l)) |
        0x90...0x9f => map!(be_u8, |s| Operand::Small(s)) |
        0xa0...0xaf => map!(be_u8, |v| Operand::Variable(v))
    ) >>
    (o)
));

named!(jz8<Instruction>, do_parse!(o: apply!(arg, 0x80) >> b: branch >> (Jz(o, b))));
named!(jz9<Instruction>, do_parse!(o: apply!(arg, 0x90) >> b: branch >> (Jz(o, b))));
named!(jza<Instruction>, do_parse!(o: apply!(arg, 0xa0) >> b: branch >> (Jz(o, b))));

named!(get_sibling8<Instruction>, do_parse!(o: apply!(arg, 0x81) >> s: store >> b: branch >> (GetSibling(o, s, b))));
named!(get_sibling9<Instruction>, do_parse!(o: apply!(arg, 0x91) >> s: store >> b: branch >> (GetSibling(o, s, b))));
named!(get_siblinga<Instruction>, do_parse!(o: apply!(arg, 0xa1) >> s: store >> b: branch >> (GetSibling(o, s, b))));

named!(get_child8<Instruction>, do_parse!(o: apply!(arg, 0x82) >> s: store >> b: branch >> (GetChild(o, s, b))));
named!(get_child9<Instruction>, do_parse!(o: apply!(arg, 0x92) >> s: store >> b: branch >> (GetChild(o, s, b))));
named!(get_childa<Instruction>, do_parse!(o: apply!(arg, 0xa2) >> s: store >> b: branch >> (GetChild(o, s, b))));

named!(get_parent8<Instruction>, do_parse!(o: apply!(arg, 0x83) >> s: store >> (GetParent(o, s))));
named!(get_parent9<Instruction>, do_parse!(o: apply!(arg, 0x93) >> s: store >> (GetParent(o, s))));
named!(get_parenta<Instruction>, do_parse!(o: apply!(arg, 0xa3) >> s: store >> (GetParent(o, s))));

// named!(inc8<Instruction>, do_parse!(o: apply!(arg, 0x85) >> (Inc(o))));
named!(inc9<Instruction>, do_parse!(o: apply!(arg, 0x95) >> (Inc(o.to_variable()))));
named!(inca<Instruction>, do_parse!(o: apply!(arg, 0xa5) >> (Inc(o))));

// named!(dec8<Instruction>, do_parse!(o: apply!(arg, 0x86) >> (Dec(o))));
named!(dec9<Instruction>, do_parse!(o: apply!(arg, 0x96) >> (Dec(o.to_variable()))));
named!(deca<Instruction>, do_parse!(o: apply!(arg, 0xa6) >> (Dec(o))));

named!(print_obj8<Instruction>, do_parse!(o: apply!(arg, 0x8a) >> (PrintObj(o))));
named!(print_obj9<Instruction>, do_parse!(o: apply!(arg, 0x9a) >> (PrintObj(o))));
named!(print_obja<Instruction>, do_parse!(o: apply!(arg, 0xaa) >> (PrintObj(o))));

named!(ret8<Instruction>, do_parse!(o: apply!(arg, 0x8b) >> (Ret(o))));
named!(ret9<Instruction>, do_parse!(o: apply!(arg, 0x9b) >> (Ret(o))));
named!(reta<Instruction>, do_parse!(o: apply!(arg, 0xab) >> (Ret(o))));

named!(jump8<Instruction>, do_parse!(o: apply!(arg, 0x8c) >> (Jump(o))));
named!(jump9<Instruction>, do_parse!(o: apply!(arg, 0x9c) >> (Jump(o))));
named!(jumpa<Instruction>, do_parse!(o: apply!(arg, 0xac) >> (Jump(o))));

named!(print_paddr8<Instruction>, do_parse!(o: apply!(arg, 0x8d) >> (PrintPaddr(o))));
named!(print_paddr9<Instruction>, do_parse!(o: apply!(arg, 0x9d) >> (PrintPaddr(o))));
named!(print_paddra<Instruction>, do_parse!(o: apply!(arg, 0xad) >> (PrintPaddr(o))));

named!(pub one_op<Instruction>, alt!(
    jz8 | jz9 | jza |
    get_sibling8 | get_sibling9 | get_siblinga |
    get_child8 | get_child9 | get_childa |
    get_parent8 | get_parent9 | get_parenta |
    inc9 | inca | dec9 | deca |
    print_obj8 | print_obj9 | print_obja |
    ret8 | ret9 | reta |
    jump8 | jump9 | jumpa |
    print_paddr8 | print_paddr9 | print_paddra
));

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::instructions::{Branch, Variable};

    #[test]
    fn one_op_variable_works() {
        assert_eq!(
            one_op(&[0xA0, 0x05, 0x4F]),
            Ok((&[][..], Jz(Variable(5).into(), Branch{condition:false, offset:15})))
        );
        assert_eq!(
            one_op(&[0xA5, 0x05]),
            Ok((&[][..], Inc(Variable(5).into())))
        );
        assert_eq!(
            one_op(&[0xA6, 0x05]),
            Ok((&[][..], Dec(Variable(5).into())))
        );
    }
}
