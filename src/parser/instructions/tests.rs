use super::*;
use super::Store;
use super::Operand::*;
use super::Operand::Variable;
use super::Instruction::*;

#[test]
fn instruction_parsing_works_czech() {
    let bytes = &include_bytes!("../../../test_data/czech.z3")[2083..2100];
    let (_, res) = instructions(bytes).unwrap();
    assert_eq!(res, vec![
        Call(Large(4356), vec![], Store(255)),
        Quit
    ]);
}

#[test]
fn instruction_parsing_works_zork1() {
    let bytes = &include_bytes!("../../../test_data/zork1.z3")[20229..20429];
    let (_, res) = instructions(bytes).unwrap();
    assert_eq!(&res[..8], &[
        Call(Large(10809), vec![Large(32784), Large(65535)], Store(0)),
        StoreW(Variable(0), Small(0), Small(1)),
        Call(Large(10809), vec![Large(32892), Large(65535)], Store(0)),
        Call(Large(10809), vec![Large(33008), Large(65535)], Store(0)),
        StoreW(Variable(0), Small(0), Small(1)),
        Call(Large(10809), vec![Large(28522), Small(40)], Store(0)),
        Call(Large(10809), vec![Large(28501), Small(200)], Store(0)),
        PutProp(Small(156), Small(6), Small(4))
    ]);
}
