use crate::parser::instructions::{branch, op4, store, Instruction, Operand};
use crate::parser::instructions::Instruction::*;
use crate::parser::instructions::Operand::*;
use nom::{be_u8};

named_args!(args(opcode: u8)<[Operand; 2]>, do_parse!(
    tag!([opcode]) >> args: count!(be_u8, 2) >>
    ({
        match opcode {
            0x00...0x1f => [Small(args[0]), Small(args[1])],
            0x20...0x3f => [Small(args[0]), Variable(args[1])],
            0x40...0x5f => [Variable(args[0]), Small(args[1])],
            0x60...0x7f => [Variable(args[0]), Variable(args[1])],
            _ => panic!("Invalid opcode"),
        }
    })
));

named!(je0<Instruction>, do_parse!(a: apply!(args, 0x01) >> b: branch >> (Je(a[0], a[1], b))));
named!(je2<Instruction>, do_parse!(a: apply!(args, 0x21) >> b: branch >> (Je(a[0], a[1], b))));
named!(je4<Instruction>, do_parse!(a: apply!(args, 0x41) >> b: branch >> (Je(a[0], a[1], b))));
named!(je6<Instruction>, do_parse!(a: apply!(args, 0x61) >> b: branch >> (Je(a[0], a[1], b))));
named!(jec<Instruction>, do_parse!(tag!([0xc1]) >> a: op4 >> b: branch >> (Je(a[0], a[1], b))));

named!(jg0<Instruction>, do_parse!(a: apply!(args, 0x03) >> b: branch >> (Jg(a[0], a[1], b))));
named!(jg2<Instruction>, do_parse!(a: apply!(args, 0x23) >> b: branch >> (Jg(a[0], a[1], b))));
named!(jg4<Instruction>, do_parse!(a: apply!(args, 0x43) >> b: branch >> (Jg(a[0], a[1], b))));
named!(jg6<Instruction>, do_parse!(a: apply!(args, 0x63) >> b: branch >> (Jg(a[0], a[1], b))));

// explicitly cast first operant to be a variable ??
named!(dec_chk0<Instruction>, do_parse!(a: apply!(args, 0x04) >> b: branch >> (DecChk(a[0].to_variable(), a[1], b))));
// explicitly cast first operant to be a variable ??
named!(inc_chk0<Instruction>, do_parse!(a: apply!(args, 0x05) >> b: branch >> (IncChk(a[0].to_variable(), a[1], b))));

named!(jin0<Instruction>, do_parse!(a: apply!(args, 0x06) >> b: branch >> (Jin(a[0], a[1], b))));
named!(jin2<Instruction>, do_parse!(a: apply!(args, 0x26) >> b: branch >> (Jin(a[0], a[1], b))));
named!(jin4<Instruction>, do_parse!(a: apply!(args, 0x46) >> b: branch >> (Jin(a[0], a[1], b))));
named!(jin6<Instruction>, do_parse!(a: apply!(args, 0x66) >> b: branch >> (Jin(a[0], a[1], b))));

named!(test0<Instruction>, do_parse!(a: apply!(args, 0x07) >> b: branch >> (Test(a[0], a[1], b))));
named!(test2<Instruction>, do_parse!(a: apply!(args, 0x27) >> b: branch >> (Test(a[0], a[1], b))));
named!(test4<Instruction>, do_parse!(a: apply!(args, 0x47) >> b: branch >> (Test(a[0], a[1], b))));
named!(test6<Instruction>, do_parse!(a: apply!(args, 0x67) >> b: branch >> (Test(a[0], a[1], b))));

named!(or0<Instruction>, do_parse!(a: apply!(args, 0x08) >> s: store >> (Or(a[0], a[1], s))));
named!(or2<Instruction>, do_parse!(a: apply!(args, 0x28) >> s: store >> (Or(a[0], a[1], s))));
named!(or4<Instruction>, do_parse!(a: apply!(args, 0x48) >> s: store >> (Or(a[0], a[1], s))));
named!(or6<Instruction>, do_parse!(a: apply!(args, 0x68) >> s: store >> (Or(a[0], a[1], s))));

named!(and0<Instruction>, do_parse!(a: apply!(args, 0x09) >> s: store >> (And(a[0], a[1], s))));
named!(and2<Instruction>, do_parse!(a: apply!(args, 0x29) >> s: store >> (And(a[0], a[1], s))));
named!(and4<Instruction>, do_parse!(a: apply!(args, 0x49) >> s: store >> (And(a[0], a[1], s))));
named!(and6<Instruction>, do_parse!(a: apply!(args, 0x69) >> s: store >> (And(a[0], a[1], s))));
named!(andc<Instruction>, do_parse!(tag!([0xc9]) >> a: op4 >> s: store >> (And(a[0], a[1], s))));

named!(test_attr0<Instruction>, do_parse!(a: apply!(args, 0x0a) >> b: branch >> (TestAttr(a[0], a[1], b))));
named!(test_attr2<Instruction>, do_parse!(a: apply!(args, 0x2a) >> b: branch >> (TestAttr(a[0], a[1], b))));
named!(test_attr4<Instruction>, do_parse!(a: apply!(args, 0x4a) >> b: branch >> (TestAttr(a[0], a[1], b))));
named!(test_attr6<Instruction>, do_parse!(a: apply!(args, 0x6a) >> b: branch >> (TestAttr(a[0], a[1], b))));

named!(set_attr0<Instruction>, do_parse!(a: apply!(args, 0x0b) >> (SetAttr(a[0], a[1]))));
named!(set_attr2<Instruction>, do_parse!(a: apply!(args, 0x2b) >> (SetAttr(a[0], a[1]))));
named!(set_attr4<Instruction>, do_parse!(a: apply!(args, 0x4b) >> (SetAttr(a[0], a[1]))));
named!(set_attr6<Instruction>, do_parse!(a: apply!(args, 0x6b) >> (SetAttr(a[0], a[1]))));

named!(clear_attr0<Instruction>, do_parse!(a: apply!(args, 0x0c) >> (ClearAttr(a[0], a[1]))));
named!(clear_attr2<Instruction>, do_parse!(a: apply!(args, 0x2c) >> (ClearAttr(a[0], a[1]))));
named!(clear_attr4<Instruction>, do_parse!(a: apply!(args, 0x4c) >> (ClearAttr(a[0], a[1]))));
named!(clear_attr6<Instruction>, do_parse!(a: apply!(args, 0x6c) >> (ClearAttr(a[0], a[1]))));

named!(store0<Instruction>, do_parse!(tag!([0x0d]) >> v: be_u8 >> s: be_u8 >> (Store(Variable(v), Small(s)))));
named!(store2<Instruction>, do_parse!(tag!([0x2d]) >> v: be_u8 >> s: be_u8 >> (Store(Variable(v), Variable(s)))));
// cast first operand to variable ??
named!(storec<Instruction>, do_parse!(tag!([0xcd]) >> a: op4 >> (Store(a[0].to_variable(), a[1]))));

named!(insert_obj0<Instruction>, do_parse!(a: apply!(args, 0x0e) >> (InsertObj(a[0], a[1]))));
named!(insert_obj2<Instruction>, do_parse!(a: apply!(args, 0x2e) >> (InsertObj(a[0], a[1]))));
named!(insert_obj4<Instruction>, do_parse!(a: apply!(args, 0x4e) >> (InsertObj(a[0], a[1]))));
named!(insert_obj6<Instruction>, do_parse!(a: apply!(args, 0x6e) >> (InsertObj(a[0], a[1]))));

named!(loadw0<Instruction>, do_parse!(a: apply!(args, 0x0f) >> s: store >> (LoadW(a[0], a[1], s))));
named!(loadw2<Instruction>, do_parse!(a: apply!(args, 0x2f) >> s: store >> (LoadW(a[0], a[1], s))));
named!(loadw4<Instruction>, do_parse!(a: apply!(args, 0x4f) >> s: store >> (LoadW(a[0], a[1], s))));
named!(loadw6<Instruction>, do_parse!(a: apply!(args, 0x6f) >> s: store >> (LoadW(a[0], a[1], s))));

named!(loadb0<Instruction>, do_parse!(a: apply!(args, 0x10) >> s: store >> (LoadB(a[0], a[1], s))));
named!(loadb2<Instruction>, do_parse!(a: apply!(args, 0x30) >> s: store >> (LoadB(a[0], a[1], s))));
named!(loadb4<Instruction>, do_parse!(a: apply!(args, 0x50) >> s: store >> (LoadB(a[0], a[1], s))));
named!(loadb6<Instruction>, do_parse!(a: apply!(args, 0x70) >> s: store >> (LoadB(a[0], a[1], s))));

named!(get_prop0<Instruction>, do_parse!(a: apply!(args, 0x11) >> s: store >> (GetProp(a[0], a[1], s))));
named!(get_prop2<Instruction>, do_parse!(a: apply!(args, 0x31) >> s: store >> (GetProp(a[0], a[1], s))));
named!(get_prop4<Instruction>, do_parse!(a: apply!(args, 0x51) >> s: store >> (GetProp(a[0], a[1], s))));
named!(get_prop6<Instruction>, do_parse!(a: apply!(args, 0x71) >> s: store >> (GetProp(a[0], a[1], s))));

named!(get_prop_addr0<Instruction>, do_parse!(a: apply!(args, 0x12) >> s: store >> (GetPropAddr(a[0], a[1], s))));
named!(get_prop_addr2<Instruction>, do_parse!(a: apply!(args, 0x32) >> s: store >> (GetPropAddr(a[0], a[1], s))));
named!(get_prop_addr4<Instruction>, do_parse!(a: apply!(args, 0x52) >> s: store >> (GetPropAddr(a[0], a[1], s))));
named!(get_prop_addr6<Instruction>, do_parse!(a: apply!(args, 0x72) >> s: store >> (GetPropAddr(a[0], a[1], s))));

named!(add0<Instruction>, do_parse!(a: apply!(args, 0x14) >> s: store >> (Add(a[0], a[1], s))));
named!(add2<Instruction>, do_parse!(a: apply!(args, 0x34) >> s: store >> (Add(a[0], a[1], s))));
named!(add4<Instruction>, do_parse!(a: apply!(args, 0x54) >> s: store >> (Add(a[0], a[1], s))));
named!(add6<Instruction>, do_parse!(a: apply!(args, 0x74) >> s: store >> (Add(a[0], a[1], s))));

named!(sub0<Instruction>, do_parse!(a: apply!(args, 0x15) >> s: store >> (Sub(a[0], a[1], s))));
named!(sub2<Instruction>, do_parse!(a: apply!(args, 0x35) >> s: store >> (Sub(a[0], a[1], s))));
named!(sub4<Instruction>, do_parse!(a: apply!(args, 0x55) >> s: store >> (Sub(a[0], a[1], s))));
named!(sub6<Instruction>, do_parse!(a: apply!(args, 0x75) >> s: store >> (Sub(a[0], a[1], s))));

named!(mul0<Instruction>, do_parse!(a: apply!(args, 0x16) >> s: store >> (Mul(a[0], a[1], s))));
named!(mul2<Instruction>, do_parse!(a: apply!(args, 0x36) >> s: store >> (Mul(a[0], a[1], s))));
named!(mul4<Instruction>, do_parse!(a: apply!(args, 0x56) >> s: store >> (Mul(a[0], a[1], s))));
named!(mul6<Instruction>, do_parse!(a: apply!(args, 0x76) >> s: store >> (Mul(a[0], a[1], s))));

named!(div0<Instruction>, do_parse!(a: apply!(args, 0x17) >> s: store >> (Div(a[0], a[1], s))));
named!(div2<Instruction>, do_parse!(a: apply!(args, 0x37) >> s: store >> (Div(a[0], a[1], s))));
named!(div4<Instruction>, do_parse!(a: apply!(args, 0x57) >> s: store >> (Div(a[0], a[1], s))));
named!(div6<Instruction>, do_parse!(a: apply!(args, 0x77) >> s: store >> (Div(a[0], a[1], s))));

named!(mod0<Instruction>, do_parse!(a: apply!(args, 0x18) >> s: store >> (Mod(a[0], a[1], s))));
named!(mod2<Instruction>, do_parse!(a: apply!(args, 0x38) >> s: store >> (Mod(a[0], a[1], s))));
named!(mod4<Instruction>, do_parse!(a: apply!(args, 0x58) >> s: store >> (Mod(a[0], a[1], s))));
named!(mod6<Instruction>, do_parse!(a: apply!(args, 0x78) >> s: store >> (Mod(a[0], a[1], s))));

named!(pub two_op<Instruction>, alt!(
    je0 | je2 | je4 | je6 | jec |
    jg0 | jg2 | jg4 | jg6 |
    dec_chk0 |
    inc_chk0 |
    jin0 | jin2 | jin4 | jin6 |
    test0 | test2 | test4 | test6 |
    or0 | or2 | or4 | or6 |
    and0 | and2 | and4 | and6 | andc |
    add0 | add2 | add4 | add6 |
    sub0 | sub2 | sub4 | sub6 |
    mul0 | mul2 | mul4 | mul6 |
    div0 | div2 | div4 | div6 |
    mod0 | mod2 | mod4 | mod6 |
    set_attr0 | set_attr2 | set_attr4 | set_attr6 |
    clear_attr0 | clear_attr2 | clear_attr4 | clear_attr6 |
    insert_obj0 | insert_obj2 | insert_obj4 | insert_obj6 |
    loadw0 | loadw2 | loadw4 | loadw6 |
    loadb0 | loadb2 | loadb4 | loadb6 |
    get_prop0 | get_prop2 | get_prop4 | get_prop6 |
    get_prop_addr0| get_prop_addr2 | get_prop_addr4 | get_prop_addr6 |
    store0 | store2 | storec |
    test_attr0 | test_attr2 | test_attr4 | test_attr6
));
