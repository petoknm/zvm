use crate::parser::instructions::{Instruction, store, op4};
use crate::parser::instructions::Instruction::*;

named!(call<Instruction>, do_parse!(tag!([0xE0]) >> o: op4 >> s: store >> (Call(o[0], o[1..].to_vec(), s))));
named!(storew<Instruction>, do_parse!(tag!([0xE1]) >> o: op4 >> (StoreW(o[0], o[1], o[2]))));
named!(storeb<Instruction>, do_parse!(tag!([0xE2]) >> o: op4 >> (StoreB(o[0], o[1], o[2]))));
named!(put_prop<Instruction>, do_parse!(tag!([0xE3]) >> o: op4 >> (PutProp(o[0], o[1], o[2]))));
named!(sread<Instruction>, do_parse!(tag!([0xE4]) >> o: op4 >> (Sread(o[0], o[1]))));
named!(print_char<Instruction>, do_parse!(tag!([0xE5]) >> o: op4 >> (PrintChar(o[0]))));
named!(print_num<Instruction>, do_parse!(tag!([0xE6]) >> o: op4 >> (PrintNum(o[0]))));
named!(push<Instruction>, do_parse!(tag!([0xE8]) >> o: op4 >> (Push(o[0]))));
// Cast operand to variable ??
named!(pull<Instruction>, do_parse!(tag!([0xE9]) >> o: op4 >> (Pull(o[0].to_variable()))));

named!(pub var_op<Instruction>, alt!(
    call | storew | storeb | put_prop | sread |
    print_char | print_num | push | pull
));

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::instructions::{Store, LargeConstant};

    #[test]
    fn var_op_works() {
        assert_eq!(
            var_op(&[224, 63, 17, 4, 255]),
            Ok((&[][..], Call(LargeConstant(4356).into(), vec![], Store(255))))
        );
    }
}
