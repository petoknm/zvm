use crate::parser::instructions::Instruction::*;
use crate::parser::instructions::{branch, Instruction};
use crate::parser::strings::zstring;

named!(rtrue<Instruction>, do_parse!(tag!([0xb0]) >> (Rtrue)));
named!(rfalse<Instruction>, do_parse!(tag!([0xb1]) >> (Rfalse)));
named!(print<Instruction>, do_parse!(tag!([0xb2]) >> zs: zstring >> (Print(zs))));
named!(print_ret<Instruction>, do_parse!(tag!([0xb3]) >> zs: zstring >> (PrintRet(zs))));
named!(nop<Instruction>, do_parse!(tag!([0xb4]) >> (Nop)));
named!(save<Instruction>, do_parse!(tag!([0xb5]) >> b: branch >> (Save(b))));
named!(restore<Instruction>, do_parse!(tag!([0xb6]) >> b: branch >> (Restore(b))));
named!(restart<Instruction>, do_parse!(tag!([0xb7]) >> (Restart)));
named!(ret_popped<Instruction>, do_parse!(tag!([0xb8]) >> (RetPopped)));
named!(pop<Instruction>, do_parse!(tag!([0xb9]) >> (Pop)));
named!(quit<Instruction>, do_parse!(tag!([0xba]) >> (Quit)));
named!(new_line<Instruction>, do_parse!(tag!([0xbb]) >> (NewLine)));
named!(show_status<Instruction>, do_parse!(tag!([0xbc]) >> (ShowStatus)));
named!(verify<Instruction>, do_parse!(tag!([0xbd]) >> b: branch >> (Verify(b))));

named!(pub zero_op<Instruction>, alt!(
    rtrue | rfalse | print | print_ret | nop | save | restore | restart |
    ret_popped | pop | quit | new_line | show_status | verify
));

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::instructions::Branch;
    use crate::parser::strings::{zword, Zstring};

    #[test]
    fn zero_op_works() {
        assert_eq!(zero_op(&[0xB0]), Ok((&[][..], Rtrue)));
        assert_eq!(zero_op(&[0xB1]), Ok((&[][..], Rfalse)));
        assert_eq!(
            zero_op(&[0xB2, 0xAA, 0xAA]),
            Ok((&[][..], Print(Zstring(vec![zword(10, 21, 10)]))))
        );
        assert_eq!(
            zero_op(&[0xB3, 0xAA, 0xAA]),
            Ok((&[][..], PrintRet(Zstring(vec![zword(10, 21, 10)]))))
        );
        assert_eq!(zero_op(&[0xB4]), Ok((&[][..], Nop)));
        assert_eq!(zero_op(&[0xB5, 0x4F]), Ok((&[][..], Save(Branch{condition: false, offset:15}))));
        assert_eq!(zero_op(&[0xB6, 0x4F]), Ok((&[][..], Restore(Branch{condition: false, offset:15}))));
        assert_eq!(zero_op(&[0xB7]), Ok((&[][..], Restart)));
        assert_eq!(zero_op(&[0xB8]), Ok((&[][..], RetPopped)));
        assert_eq!(zero_op(&[0xB9]), Ok((&[][..], Pop)));
        assert_eq!(zero_op(&[0xBA]), Ok((&[][..], Quit)));
        assert_eq!(zero_op(&[0xBB]), Ok((&[][..], NewLine)));
        assert_eq!(zero_op(&[0xBC]), Ok((&[][..], ShowStatus)));
        assert_eq!(zero_op(&[0xBD, 0x4F]), Ok((&[][..], Verify(Branch{condition:false, offset:15}))));
    }
}
