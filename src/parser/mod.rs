pub mod header;
pub mod strings;
pub mod dictionary;
pub mod abbreviations;
pub mod instructions;
pub mod routine;
pub mod objects;

use nom::be_u16;

named!(pub globals<[u16; 240]>, count_fixed!(u16, be_u16, 240));
