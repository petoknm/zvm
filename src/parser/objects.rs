use crate::parser::strings::{zstring, Zstring};
use nom::be_u16;

#[derive(Debug, PartialEq)]
pub struct Property<'a>(&'a mut [u8]);

impl<'a> Property<'a> {
    pub fn id(&self) -> u8 {
        let h = self.0[0];
        h % 32
    }

    pub fn data_mut(self) -> &'a mut [u8] {
        let h = self.0[0] as usize;
        let len = h / 32 + 1;
        &mut self.0[1..1+len]
    }

    pub fn next_property(self) -> Property<'a> {
        let h = self.0[0] as usize;
        let len = h / 32 + 1;
        Property(&mut self.0[1+len..])
    }
}

pub struct PropertyTable<'a>(pub &'a mut [u8]);

impl<'a> PropertyTable<'a> {
    pub fn name(&self) -> Zstring {
        let len = self.0[0] as usize * 2;
        zstring(&self.0[1..1+len]).unwrap().1
    }

    pub fn property(self, pid: u8) -> Option<Property<'a>> {
        let len = self.0[0] as usize * 2;
        let mut p = Property(&mut self.0[1+len..]);
        loop {
            if p.id() == pid {
                return Some(p);
            }
            if p.id() < pid {
                break;
            }
            p = p.next_property();
        }
        None
    }
}

#[derive(PartialEq, Debug)]
pub struct Object<'a>(&'a mut [u8]);

impl<'a> Object<'a> {
    pub fn flags_mut(self) -> &'a mut [u8] {
        &mut self.0[0..4]
    }

    pub fn parent(self) -> &'a mut u8 {
        &mut self.0[4]
    }

    pub fn sibling(self) -> &'a mut u8 {
        &mut self.0[5]
    }

    pub fn child(self) -> &'a mut u8 {
        &mut self.0[6]
    }

    pub fn property_table_addr(&self) -> u16 {
        let (_, addr) = be_u16(&self.0[7..9]).unwrap();
        addr
    }
}

pub struct ObjectTable<'a>(pub &'a mut [u8]);

impl<'a> ObjectTable<'a> {
    pub fn property_defaults(&self) -> [u16; 31] {
        named!(pd<[u16; 31]>, count_fixed!(u16, be_u16, 31));
        pd(self.0).unwrap().1
    }

    pub fn object(self, oid: u8) -> Object<'a> {
        let offset = 62 + (oid as usize - 1) * 9;
        Object(&mut self.0[offset..])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::strings::zword;

    #[test]
    fn object_table_works() {
        let mut p = include_bytes!("../../test_data/zork1.z3").to_vec();
        let ot = ObjectTable(&mut p[688..]);
        assert_eq!(
            ot.property_defaults(),
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        );
    }

    #[test]
    fn object_works() {
        let mut p = include_bytes!("../../test_data/zork1.z3").to_vec();
        {
            let o = ObjectTable(&mut p[688..]).object(156);
            assert_eq!(
                o.flags_mut(),
                &mut [0, 144, 64, 48]
            );
        }
        {
            let o = ObjectTable(&mut p[688..]).object(156);
            assert_eq!(
                o.parent(),
                &mut 0
            );
        }
        {
            let o = ObjectTable(&mut p[688..]).object(156);
            assert_eq!(
                o.property_table_addr(),
                6626
            );
        }
    }

    #[test]
    fn property_table_works() {
        let mut p = include_bytes!("../../test_data/zork1.z3").to_vec();
        let addr = ObjectTable(&mut p[688..]).object(156).property_table_addr() as usize;
        {
            let pt = PropertyTable(&mut p[addr..]);
            assert_eq!(
                pt.name(),
                Zstring(vec![
                    zword(18, 6, 12),
                    zword(14, 8, 0),
                    zword(7, 20, 6),
                    zword(25, 5, 5)
                ])
            );
        }
        {
            let pt = PropertyTable(&mut p[addr..]);
            assert!(pt.property(6).is_some());
        }
    }

    #[test]
    fn property_works() {
        let mut p = include_bytes!("../../test_data/zork1.z3").to_vec();
        let addr = ObjectTable(&mut p[688..]).object(156).property_table_addr() as usize;
        assert_eq!(PropertyTable(&mut p[addr..]).property(6).unwrap().id(), 6);
        assert_eq!(
            PropertyTable(&mut p[addr..]).property(6).unwrap().data_mut(),
            &mut [0, 4]
        );
    }
}
