use nom::{be_u8, be_u16};

#[derive(PartialEq, Debug, Clone)]
pub struct Routine {
    pub vars: Vec<u16>,
    pub pc: u16,
}

named_args!(pub routine(offset: u16)<Routine>, map!(
    length_count!(be_u8, be_u16),
    |vars| {
        let pc = offset + 2 * (vars.len() as u16) + 1;
        Routine { vars, pc }
    }
));
