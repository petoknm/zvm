#[derive(Copy, Clone, Default, Debug, PartialEq)]
pub struct Zchar(pub u8);

#[derive(Debug, PartialEq, Clone)]
pub struct Zword(pub [Zchar; 3]);

#[derive(Debug, PartialEq, Clone)]
pub struct Zstring(pub Vec<Zword>);

impl Zstring {
    pub fn to_zchars(&self) -> Vec<Zchar> {
        self.0.iter().flat_map(|zw| zw.0.iter()).cloned().collect()
    }
}

named!(zword_data_bits<(&[u8], usize), Zword>, map!(
    count_fixed!(Zchar, map!(take_bits!(u8, 5), Zchar), 3),
    Zword
));

named!(zword_ne<Zword>, bits!(preceded!(tag_bits!(u8, 1, 0), zword_data_bits)));
named!(zword_e<Zword>, bits!(preceded!(tag_bits!(u8, 1, 1), zword_data_bits)));
named!(zword_any<Zword>, bits!(preceded!(take_bits!(u8, 1), zword_data_bits)));

named_args!(pub zstring_fixed(zwords: usize)<Zstring>, do_parse!(
    zws: count!(zword_any, zwords) >>
    (Zstring(zws))
));

named!(pub zstring<Zstring>, do_parse!(
    ne: many0!(zword_ne) >>
    e: zword_e >>
    (Zstring(ne.iter().chain(&[e]).cloned().collect()))
));

pub fn zword(a: u8, b: u8, c: u8) -> Zword {
    Zword([Zchar(a), Zchar(b), Zchar(c)])
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn zword_works() {
        assert_eq!(zword_e( &[0xAA, 0xAA]), Ok((&[][..], zword(10, 21, 10))));
        assert_eq!(zword_ne(&[0x2A, 0xAA]), Ok((&[][..], zword(10, 21, 10))));
    }

    #[test]
    fn zstring_works() {
        assert_eq!(zstring(&[0x2A, 0xAA, 0xAA, 0xAA]), Ok((&[][..], Zstring(
            vec![zword(10, 21, 10), zword(10, 21, 10)]
        ))));
    }

}
