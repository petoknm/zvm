use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use nom;

use crate::parser::globals;
use crate::parser::routine::{routine, Routine};
use crate::parser::instructions::{instruction, Instruction as ParserInstruction};
use crate::parser::header::{header, Header};

#[derive(Debug)]
pub enum Error<'a> {
    Parser(nom::Err<&'a [u8]>),
    MisalignedAccess,
}

#[derive(Clone, PartialEq, Debug)]
pub struct Instruction {
    pub instr: ParserInstruction,
    pub len: u16,
}

pub struct Program {
    data: Vec<u8>,
    pub header: Header,
    instructions: RefCell<HashMap<u16, Instruction>>,
    misaligned: RefCell<HashSet<u16>>,
}

impl Program {

    pub fn new(data: Vec<u8>) -> Program {
        let (_, header) = header(&data[..]).unwrap();

        Program {
            data, header,
            instructions: RefCell::new(HashMap::new()),
            misaligned: RefCell::new(HashSet::new()),
        }
    }

    pub fn globals(&self) -> Result<[u16; 240], Error> {
        let base = self.header.global_table_base as usize;
        let (_, g) = globals(&self.data[base..]).map_err(Error::Parser)?;
        Ok(g)
    }

    pub fn memory(&self) -> &[u8] {
        &self.data[..]
    }

    pub fn dynamic_memory(&self) -> &[u8] {
        &self.data[..self.header.static_memory_base as usize]
    }

    pub fn dictionary(&self) -> &[u8] {
        &self.data[self.header.dictionary_base as usize..]
    }

    pub fn high_memory(&self) -> &[u8] {
        &self.data[self.header.high_memory_base as usize..]
    }

    pub fn routine(&self, address: u16) -> Result<Routine, Error> {
        let address = 2 * address;
        let (_, r) = routine(&self.data[address as usize ..], address).map_err(Error::Parser)?;
        Ok(r)
    }

    pub fn instruction(&self, address: u16) -> Result<Instruction, Error> {
        if let Some(_) = self.misaligned.borrow().get(&address) {
            return Err(Error::MisalignedAccess);
        }

        if let Some(i) = self.instructions.borrow().get(&address) {
            debug!("instruction cache hit {}", address);
            return Ok(i.clone());
        }

        debug!("instruction cache miss {}", address);
        let input = &self.data[(address as usize)..];
        // debug!("input {:?}", input);
        let (rest, instr) = instruction(input).map_err(Error::Parser)?;
        let len = (input.len() - rest.len()) as u16;

        {
            let mut misaligned = self.misaligned.borrow_mut();
            for addr in (address + 1..).take(len as usize - 1) {
                misaligned.insert(addr);
            }
        }

        let i = Instruction{ instr, len };
        self.instructions.borrow_mut().insert(address, i.clone());
        Ok(i)
    }

}
