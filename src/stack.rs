use std::iter::repeat;
use crate::parser::instructions::{Store};

#[derive(Debug)]
struct StackFrame {
    locals: [u16; 15],
    store: Store,
    ret_addr: u16,
}

pub struct Stack {
    frames: Vec<StackFrame>,
    stack: Vec<u16>,
}

impl Stack {
    pub fn new() -> Stack {
        Stack {
            stack: vec![],
            frames: vec![StackFrame {
                locals: [0; 15],
                store: Store(0),
                ret_addr: 0,
            }]
        }
    }

    pub fn call(&mut self, ret_addr: u16, args_init: Vec<u16>, args: Vec<u16>, store: Store) {
        let locals_vec: Vec<_> = args.iter()
            .chain(args_init.iter().skip(args.len()))
            .cloned()
            .chain(repeat(0))
            .take(15)
            .collect();
        let locals = cascade!{
            [0; 15];
            ..clone_from_slice(&locals_vec[..]);
        };
        self.frames.push(StackFrame { locals, store, ret_addr });
        debug!("new stack frame {:?}", self.frames.last().unwrap());
    }

    // returns (store, return_address)
    pub fn ret(&mut self) -> (Store, u16) {
        let f = self.frames.pop().unwrap();
        (f.store, f.ret_addr)
    }

    pub fn read_local(&self, n: u8) -> u16 {
        let v = self.frames.last().unwrap().locals[n as usize - 1];
        debug!("read local l({})={}", n, v);
        v
    }

    pub fn write_local(&mut self, n: u8, v: u16) {
        debug!("write local l({})={}", n, v);
        self.frames.last_mut().unwrap().locals[n as usize - 1] = v;
    }

    pub fn push(&mut self, v: u16) {
        debug!("push {}", v);
        self.stack.push(v);
    }

    pub fn pop(&mut self) -> u16 {
        let v = self.stack.pop().unwrap();
        debug!("pop {}", v);
        v
    }
}
