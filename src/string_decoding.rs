use std::fmt::Write;

use crate::alphabet::Alphabet;
use crate::parser::strings::{Zchar, Zstring, zstring};
use crate::parser::abbreviations::abbrev_table;

use nom;

#[derive(Clone)]
pub struct AbbrevInfo<'a>{
    pub dynamic_memory: &'a [u8],
    pub abbrev_table_base: u16,
}

type NomError<'a> = nom::Err<&'a [u8]>;

#[derive(Debug, PartialEq, Error)]
pub enum Error<'a> {
    Parser(NomError<'a>),
    Fmt(std::fmt::Error),
    AbbrevNotAllowed,
}

enum State {
    Alphabet(u8),
    Abbrev(u8),
    TenBit1,
    TenBit2(u8),
}

fn decode_impl<'a>(
    zchars: &[Zchar],
    alphabet: &Alphabet,
    abbrev_info: Option<AbbrevInfo<'a>>
) -> Result<String, Error<'a>> {
    use self::State::*;
    debug!("decode_impl {:?}", zchars);

    let mut state = Alphabet(0);
    let mut res = String::new();
    for zc in zchars {
        state = match (state, zc.0) {
            (_, c @ 1..=3) => Abbrev(c),
            (_, 4) => Alphabet(1),
            (_, 5) => Alphabet(2),
            (Alphabet(2), 6) => TenBit1,
            (TenBit1, c) => TenBit2(c),

            (TenBit2(c1), c2) => {
                let c = (c1 as u16) << 5 | c2 as u16;
                let c: char = (c as u8).into();
                write!(res, "{}", c)?;
                Alphabet(0)
            }
            (Abbrev(z), x) => {
                let AbbrevInfo {
                    dynamic_memory,
                    abbrev_table_base
                } = abbrev_info.clone().ok_or(Error::AbbrevNotAllowed)?;

                let (_, abbrev_table) = abbrev_table(&dynamic_memory[abbrev_table_base as usize..])?;
                let w_addr = abbrev_table.entries[32*(z as usize - 1) + x as usize] as usize;
                let (_, zs) = zstring(&dynamic_memory[2*w_addr..])?;
                let s = decode_impl(&zs.to_zchars(), alphabet, None)?;
                write!(res, "{}", s)?;

                Alphabet(0)
            }
            (Alphabet(a), c) => {
                let c = alphabet.index(a, c);
                write!(res, "{}", c)?;
                Alphabet(0)
            }
        }
    }

    debug!("decode_impl res={}", res);
    Ok(res)
}

pub fn decode<'a>(
    zs: &Zstring,
    alphabet: &Alphabet,
    abbrev_info: Option<AbbrevInfo<'a>>
) -> Result<String, Error<'a>> {
    decode_impl(&zs.to_zchars(), alphabet, abbrev_info)
}

pub fn decode_addr<'a>(
    addr: u16, high_memory: &'a[u8],
    alphabet: &Alphabet,
    abbrev_info: Option<AbbrevInfo<'a>>
) -> Result<String, Error<'a>> {
    let (_, zs) = zstring(&high_memory[addr as usize..])?;
    decode(&zs, alphabet, abbrev_info)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::parser::strings::zword;

    #[test]
    fn zstring_decoding_works() {
        let zs = &Zstring(vec![zword(6, 8, 23), zword(20, 24, 24)]);
        let a = &Alphabet::default();
        assert_eq!(
            decode(zs, a, None),
            Ok("across".to_string())
        );
    }
}
