use nom::be_u16;
use std::convert::TryFrom;
use std::io::Write;

use crate::string_decoding::{AbbrevInfo, decode, decode_addr};
use crate::alphabet::Alphabet;
use crate::objects as obj;
use crate::parser::instructions::Instruction::Store as StoreInstruction;
use crate::parser::instructions::Instruction::*;
use crate::parser::instructions::{Branch, Operand, Store, Variable};
use crate::program::{Instruction, Program};
use crate::stack::Stack;
use crate::dictionary::Dictionary;

pub struct Zvm {
    output: Box<Write>,
    p: Program,
    alphabet: Alphabet,
    dictionary: Dictionary,
    pc: u16,
    stack: Stack,
    globals: [u16; 240],
    dynamic_memory: Vec<u8>,
}

pub enum State {
    Continue,
    WaitForInput,
    Restart,
    Quit,
}

impl Zvm {
    pub fn new(output: Box<Write>, p: Program) -> Zvm {
        let globals = p.globals().unwrap();
        let dynamic_memory = p.dynamic_memory().to_vec();
        let pc = p.header.pc;
        let alphabet = Alphabet::default();
        let dictionary = Dictionary::parse(&p, &alphabet);
        Zvm {
            output,
            p,
            pc,
            globals,
            dynamic_memory,
            alphabet,
            dictionary,
            stack: Stack::new(),
        }
    }

    fn abbrev_info(&self) -> AbbrevInfo {
        AbbrevInfo{
            dynamic_memory: &self.dynamic_memory,
            abbrev_table_base: self.p.header.abbrev_table_base
        }
    }

    pub fn run(&mut self, mut input: Option<String>) -> State {
        loop {
            match self.handle_instruction(self.p.instruction(self.pc).unwrap(), &mut input) {
                s @ State::WaitForInput => return s,
                s @ State::Quit => return s,
                _ => {}
            }
        }
    }

    fn analyze(&mut self, input: &str, parse_addr: usize) {
        let input = self.dictionary.word_separators.iter().cloned().fold(
            input.to_string(),
            |input, sep| input.replace(sep, &format!(" {} ", sep))
        );

        let words: Vec<_> = input.split_whitespace().map(String::from).collect();

        let max_indexes = self.dynamic_memory[parse_addr];
        let entries: Vec<_> = words.iter()
            .map(|w| self.dictionary.find(w).unwrap_or_default())
            .take(max_indexes as usize)
            .collect();

        let mut parse_buf = cascade!{
            Vec::new();
            ..push(max_indexes);
            ..push(entries.len() as u8);
        };

        for e in entries {
            use bytes::BufMut;

            parse_buf.put_u16_be(e.address);
            parse_buf.push(e.text.len() as u8);
            parse_buf.push(0); // TODO original start position in input
        }

        (&mut self.dynamic_memory[parse_addr..])
            .write(&parse_buf)
            .unwrap();
    }

    fn handle_instruction(&mut self, Instruction{instr, len}: Instruction, input: &mut Option<String>) -> State {
        debug!("handle {:?}", instr);
        self.pc += len;

        let s = match instr {
            Nop => State::Continue,
            Sread(text_addr, parse_addr) => match input.take() {
                Some(input) => {
                    let text_addr = self.resolve_operand(text_addr) as usize;
                    let parse_addr = self.resolve_operand(parse_addr) as usize;

                    if parse_addr != 0 {
                        self.analyze(&input, parse_addr);
                    }

                    let max_len = self.dynamic_memory[text_addr] as usize;
                    let input = cascade!{
                        (&input[..std::cmp::min(input.len(), max_len - 1)])
                        .to_lowercase()
                        .as_bytes()
                        .to_vec();
                        ..push(0);
                    };

                    (&mut self.dynamic_memory[text_addr + 1..])
                        .write(&input)
                        .unwrap();

                    State::Continue
                }
                _ => State::WaitForInput,
            },
            Call(addr, args, st) => {
                let addr = self.resolve_operand(addr);
                let r = self.p.routine(addr).unwrap();
                debug!("calling {:?} with args {:?}", r, args);
                let args = args.iter().map(|&o| self.resolve_operand(o)).collect();
                self.stack.call(self.pc, r.vars, args, st);
                self.pc = r.pc;
                State::Continue
            }
            Ret(r) => {
                let r = self.resolve_operand(r);
                let (st, pc) = self.stack.ret();
                self.handle_store(st, r);
                self.pc = pc;
                debug!("ret pc={}, v={}", pc, r);
                State::Continue
            }
            Rtrue => {
                let (st, pc) = self.stack.ret();
                self.handle_store(st, 1);
                self.pc = pc;
                debug!("rtrue pc={}", pc);
                State::Continue
            }
            Rfalse => {
                let (st, pc) = self.stack.ret();
                self.handle_store(st, 0);
                self.pc = pc;
                debug!("rfalse pc={}", pc);
                State::Continue
            }
            RetPopped => {
                let v = self.stack.pop();
                self.handle_instruction(
                    Instruction {
                        instr: Ret(Operand::Large(v)),
                        len: 0,
                    },
                    input,
                );
                State::Continue
            }
            Je(a, b, br) => {
                let a = self.resolve_operand(a);
                let b = self.resolve_operand(b);
                let c = a == b;
                debug!("je a={}, b={}, c={}", a, b, c);
                self.handle_branch(br, c);
                State::Continue
            }
            Jg(a, b, br) => {
                let a = self.resolve_operand(a);
                let b = self.resolve_operand(b);
                let c = a > b;
                debug!("jg a={}, b={}, c={}", a, b, c);
                self.handle_branch(br, c);
                State::Continue
            }
            Jz(a, br) => {
                let a = self.resolve_operand(a);
                let c = a == 0;
                debug!("jz a={}, c={}", a, c);
                self.handle_branch(br, c);
                State::Continue
            }
            Jump(o) => {
                let o = self.resolve_operand(o) as i16;
                debug!("jump {}", o);
                self.handle_branch(
                    Branch {
                        condition: true,
                        offset: o,
                    },
                    true,
                );
                State::Continue
            }
            Jin(a, b, br) => {
                let a = self.resolve_operand(a) as u8;
                let b = self.resolve_operand(b) as u8;
                let v = obj::is_parent_of(&mut self.dynamic_memory, &self.p.header, b, a);
                debug!("jin a={}, b={}, v={}", a, b, v);
                self.handle_branch(br, v);
                State::Continue
            }
            IncChk(Operand::Variable(v), o, br) => {
                let val = self.resolve_operand(Operand::Variable(v)) as i16;
                let val = val.wrapping_add(1);
                let o = self.resolve_operand(o) as i16;
                self.handle_store(Store(v), val as u16);
                self.handle_branch(br, val > o);
                State::Continue
            }
            Inc(Operand::Variable(v)) => {
                let val = self.resolve_operand(Operand::Variable(v)).wrapping_add(1);
                self.handle_store(Store(v), val);
                State::Continue
            }
            DecChk(Operand::Variable(v), o, br) => {
                let val = self.resolve_operand(Operand::Variable(v)) as i16;
                let val = val.wrapping_sub(1);
                let o = self.resolve_operand(o) as i16;
                self.handle_store(Store(v), val as u16);
                self.handle_branch(br, val < o);
                State::Continue
            }
            Dec(Operand::Variable(v)) => {
                let val = self.resolve_operand(Operand::Variable(v)).wrapping_sub(1);
                self.handle_store(Store(v), val);
                State::Continue
            }
            Test(bitmap, flags, b) => {
                let bitmap = self.resolve_operand(bitmap);
                let flags = self.resolve_operand(flags);
                let c = (bitmap & flags) == flags;
                self.handle_branch(b, c);
                State::Continue
            }
            Or(a, b, st) => {
                let a = self.resolve_operand(a);
                let b = self.resolve_operand(b);
                let c = a | b;
                self.handle_store(st, c);
                State::Continue
            }
            And(a, b, st) => {
                let a = self.resolve_operand(a);
                let b = self.resolve_operand(b);
                let c = a & b;
                self.handle_store(st, c);
                State::Continue
            }
            Add(a, b, st) => {
                let a = self.resolve_operand(a) as i16;
                let b = self.resolve_operand(b) as i16;
                let c = a.wrapping_add(b);
                debug!("add {} + {} = {}", a, b, c);
                self.handle_store(st, c as u16);
                State::Continue
            }
            Sub(a, b, st) => {
                let a = self.resolve_operand(a) as i16;
                let b = self.resolve_operand(b) as i16;
                let c = a.wrapping_sub(b);
                debug!("sub {} - {} = {}", a, b, c);
                self.handle_store(st, c as u16);
                State::Continue
            }
            Mul(a, b, st) => {
                let a = self.resolve_operand(a) as i16;
                let b = self.resolve_operand(b) as i16;
                let c = a * b;
                debug!("mul {} * {} = {}", a, b, c);
                self.handle_store(st, c as u16);
                State::Continue
            }
            Div(a, b, st) => {
                let a = self.resolve_operand(a) as i16;
                let b = self.resolve_operand(b) as i16;
                let c = a / b;
                debug!("div {} / {} = {}", a, b, c);
                self.handle_store(st, c as u16);
                State::Continue
            }
            Mod(a, b, st) => {
                let a = self.resolve_operand(a) as i16;
                let b = self.resolve_operand(b) as i16;
                let c = a % b;
                debug!("mod {} % {} = {}", a, b, c);
                self.handle_store(st, c as u16);
                State::Continue
            }
            StoreW(arr, wi, v) => {
                let arr = self.resolve_operand(arr);
                let wi = self.resolve_operand(wi);
                let v = self.resolve_operand(v);

                let addr = arr + 2 * wi;
                assert!(addr < self.p.header.static_memory_base);

                let addr = addr as usize;
                self.dynamic_memory[addr] = (v >> 8) as u8;
                self.dynamic_memory[addr + 1] = v as u8;
                State::Continue
            }
            StoreB(arr, bi, v) => {
                let arr = self.resolve_operand(arr);
                let bi = self.resolve_operand(bi);
                let v = self.resolve_operand(v) as u8;

                let addr = arr + bi;
                assert!(addr < self.p.header.static_memory_base);

                self.dynamic_memory[addr as usize] = v;
                State::Continue
            }
            LoadW(arr, wi, st) => {
                let arr = self.resolve_operand(arr);
                let wi = self.resolve_operand(wi);

                let addr = arr + 2 * wi;
                assert!(addr < self.p.header.static_memory_base);

                let addr = addr as usize;
                let (_, v) = be_u16(&self.dynamic_memory[addr..]).unwrap();
                self.handle_store(st, v);
                State::Continue
            }
            LoadB(arr, idx, st) => {
                let arr = self.resolve_operand(arr);
                let idx = self.resolve_operand(idx);

                let addr = arr + idx;
                let addr = addr as usize;

                let v = if addr < self.p.header.static_memory_base as usize {
                    self.dynamic_memory[addr]
                } else {
                    self.p.memory()[addr]
                } as u16;

                self.handle_store(st, v);
                State::Continue
            }
            StoreInstruction(Operand::Variable(v), val) => {
                let val = self.resolve_operand(val);
                self.handle_store(Store(v), val);
                State::Continue
            }
            Push(val) => {
                let val = self.resolve_operand(val);
                self.handle_store(Store(0), val);
                State::Continue
            }
            Pull(Operand::Variable(v)) => {
                let val = self.resolve_operand(Operand::Variable(0));
                self.handle_store(Store(v), val);
                State::Continue
            }
            GetParent(oid, s) => {
                let oid = self.resolve_operand(oid) as u8;
                let v = obj::get_parent(&mut self.dynamic_memory, &self.p.header, oid);
                self.handle_store(s, v as u16);
                State::Continue
            }
            GetChild(oid, s, b) => {
                let oid = self.resolve_operand(oid) as u8;
                let c_oid = obj::get_child(&mut self.dynamic_memory, &self.p.header, oid);
                self.handle_store(s, c_oid as u16);
                self.handle_branch(b, c_oid != 0);
                State::Continue
            }
            GetSibling(oid, s, b) => {
                let oid = self.resolve_operand(oid) as u8;
                let s_oid = obj::get_sibling(&mut self.dynamic_memory, &self.p.header, oid);
                self.handle_store(s, s_oid as u16);
                self.handle_branch(b, s_oid != 0);
                State::Continue
            }
            InsertObj(s_oid, d_oid) => {
                let s_oid = self.resolve_operand(s_oid) as u8;
                let d_oid = self.resolve_operand(d_oid) as u8;
                obj::insert(&mut self.dynamic_memory, &self.p.header, s_oid, d_oid);
                State::Continue
            }
            GetProp(oid, pid, s) => {
                let oid = self.resolve_operand(oid) as u8;
                let pid = self.resolve_operand(pid) as u8;
                let v = if let Some(data) =
                    obj::get_property_data_mut(&mut self.dynamic_memory, &self.p.header, oid, pid)
                {
                    match data.len() {
                        0 => 0, // ??
                        1 => data[0] as u16,
                        2 => be_u16(data).unwrap().1,
                        _ => unimplemented!(),
                    }
                } else {
                    obj::get_property_default(&mut self.dynamic_memory, &self.p.header, pid)
                };
                self.handle_store(s, v);
                State::Continue
            }
            GetPropAddr(oid, pid, s) => {
                let oid = self.resolve_operand(oid) as u8;
                let pid = self.resolve_operand(pid) as u8;
                let v = obj::get_property_data_addr(
                    &mut self.dynamic_memory,
                    &self.p.header,
                    oid,
                    pid,
                )
                .unwrap_or(0);
                self.handle_store(s, v);
                State::Continue
            }
            PutProp(oid, pid, v) => {
                let oid = self.resolve_operand(oid) as u8;
                let pid = self.resolve_operand(pid);
                let v = self.resolve_operand(v);

                let data = obj::get_property_data_mut(
                    &mut self.dynamic_memory,
                    &self.p.header,
                    oid,
                    pid as u8,
                )
                .unwrap();
                match data.len() {
                    0 => unimplemented!(), // ??
                    1 => data[0] = v as u8,
                    2 => {
                        data[0] = (v >> 8) as u8;
                        data[1] = v as u8;
                    }
                    _ => unimplemented!(),
                }
                State::Continue
            }
            TestAttr(oid, aid, br) => {
                let oid = self.resolve_operand(oid) as u8;
                let aid = self.resolve_operand(aid) as usize;
                debug!("test_attr oid={}, aid={}", oid, aid);
                let v = obj::get_attr(&mut self.dynamic_memory, &self.p.header, oid, aid);
                self.handle_branch(br, v);
                State::Continue
            }
            SetAttr(oid, aid) => {
                let oid = self.resolve_operand(oid) as u8;
                let aid = self.resolve_operand(aid) as usize;
                obj::set_attr(&mut self.dynamic_memory, &self.p.header, oid, aid, true);
                State::Continue
            }
            ClearAttr(oid, aid) => {
                let oid = self.resolve_operand(oid) as u8;
                let aid = self.resolve_operand(aid) as usize;
                obj::set_attr(&mut self.dynamic_memory, &self.p.header, oid, aid, false);
                State::Continue
            }
            NewLine => {
                write!(self.output, "\n").unwrap();
                State::Continue
            }
            Print(zs) => {
                let s = decode(
                    &zs,
                    &self.alphabet,
                    Some(self.abbrev_info())
                ).unwrap();
                debug!("print \"{}\"", s);
                write!(self.output, "{}", &s).unwrap();
                State::Continue
            }
            PrintChar(o) => {
                let o = self.resolve_operand(o) as u8;
                let c: char = o.into();
                debug!("print_char zscii={}, c={}", o, c);
                write!(self.output, "{}", c).unwrap();
                State::Continue
            }
            PrintNum(o) => {
                let o = self.resolve_operand(o) as i16;
                let s = format!("{}", o);
                debug!("print_num {}", o);
                write!(self.output, "{}", &s).unwrap();
                State::Continue
            }
            PrintObj(o) => {
                let o = self.resolve_operand(o) as u8;
                let zs = obj::get_name(&mut self.dynamic_memory, &self.p.header, o);
                let s = decode(
                    &zs,
                    &self.alphabet,
                    Some(self.abbrev_info())
                ).unwrap();
                debug!("print_obj o={}, s={}", o, s);
                write!(self.output, "{}", &s).unwrap();
                State::Continue
            }
            PrintPaddr(paddr) => {
                let paddr = self.resolve_operand(paddr);
                let s = decode_addr(
                    2 * paddr,
                    self.p.high_memory(),
                    &self.alphabet,
                    Some(self.abbrev_info())
                ).unwrap();
                debug!("print_paddr paddr={}, s={}", paddr, s);
                write!(self.output, "{}", &s).unwrap();
                State::Continue
            }
            Quit => State::Quit,
            i => panic!("unimplemented {:?}", i),
        };

        match s {
            // repeat instruction that is waiting for input
            State::WaitForInput => self.pc -= len,
            _ => {},
        }

        s
    }

    fn handle_store(&mut self, s: Store, v: u16) {
        let Store(s) = s;
        match s {
            0 => self.stack.push(v),
            1...15 => self.stack.write_local(s, v),
            s => self.globals[s as usize - 16] = v,
        }
    }

    fn handle_branch(&mut self, b: Branch, c: bool) {
        if b.condition != c {
            return;
        }

        match b.offset {
            0 => {
                self.handle_instruction(
                    Instruction {
                        instr: Rfalse,
                        len: 0,
                    },
                    &mut None,
                );
            }
            1 => {
                self.handle_instruction(
                    Instruction {
                        instr: Rtrue,
                        len: 0,
                    },
                    &mut None,
                );
            }
            o => self.pc = (self.pc as i32 + o as i32 - 2) as u16,
        }
    }

    fn resolve_operand(&mut self, o: Operand) -> u16 {
        match u16::try_from(o) {
            Ok(r) => r,
            Err(Variable(0)) => self.stack.pop(),
            Err(Variable(l @ 1...15)) => self.stack.read_local(l),
            Err(Variable(v)) => self.globals[v as usize - 16],
        }
    }
}

#[cfg(test)]
mod tests {
    extern crate env_logger;
    use super::*;

    // #[test]
    // fn run_czech_works() {
    //     let bytes = include_bytes!("../test_data/czech.z3");
    //     let program = Program::new(bytes.to_vec());
    //     let out = Box::new(String::new());
    //     Zvm::new(out).run(&program);
    // }

    #[test]
    fn run_zork1_works() {
        let _ = env_logger::try_init();
        let bytes = include_bytes!("../test_data/zork1.z3");
        let program = Program::new(bytes.to_vec());
        let out = Box::new(Vec::new());
        Zvm::new(out, program).run(None);
    }
}
